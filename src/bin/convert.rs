#![allow(dead_code)]
// #[macro_use(bson, doc)]
// mongod --unixSocketPrefix=./mongo --dbpath=./mongo --port=10060 --logpath=./mongo/mongod.log --pidfilepath=./mongo/mongod.pid
//
// # For MongoDB Atlas
// const MongoClient = require('mongodb').MongoClient;
// const uri = "mongodb+srv://admin:<password>@cluster0-6krqd.mongodb.net/test?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true });
// client.connect(err => {
//   const collection = client.db("test").collection("devices");
//   // perform actions on the collection object
//   client.close();
// });
//
use serde::{Deserialize, Serialize};
use serde_json::{from_value, json, Deserializer, Value};
use std::fs::File;
use std::io::*;

fn main() {
    let json = load().expect("no good");
    println!("{:?}", &json[..9]);
}

// 第7章: データベース
//
// artist.json.gzは，オープンな音楽データベースMusicBrainzの中で，アーティストに関するものをJSON形式に変換し，gzip形式で圧縮したファイルである．このファイルには，1アーティストに関する情報が1行にJSON形式で格納されている．JSON形式の概要は以下の通りである．
//
// フィールド	型	内容	例
// id	ユニーク識別子	整数	20660
// gid	グローバル識別子	文字列	"ecf9f3a3-35e9-4c58-acaa-e707fba45060"
// name	アーティスト名	文字列	"Oasis"
// sort_name	アーティスト名（辞書順整列用）	文字列	"Oasis"
// area	活動場所	文字列	"United Kingdom"
// aliases	別名	辞書オブジェクトのリスト
// aliases[].name	別名	文字列	"オアシス"
// aliases[].sort_name	別名（整列用）	文字列	"オアシス"
// begin	活動開始日	辞書
// begin.year	活動開始年	整数	1991
// begin.month	活動開始月	整数
// begin.date	活動開始日	整数
// end	活動終了日	辞書
// end.year	活動終了年	整数	2009
// end.month	活動終了月	整数	8
// end.date	活動終了日	整数	28
// tags	タグ	辞書オブジェクトのリスト
// tags[].count	タグ付けされた回数	整数	1
// tags[].value	タグ内容	文字列	"rock"
// rating	レーティング	辞書オブジェクト
// rating.count	レーティングの投票数	整数	13
// rating.value	レーティングの値（平均値）	整数	86
// artist.json.gzのデータをKey-Value-Store (KVS) およびドキュメント志向型データベースに格納・検索することを考える．KVSとしては，LevelDB，Redis，KyotoCabinet等を用いよ．ドキュメント志向型データベースとして，MongoDBを採用したが，CouchDBやRethinkDB等を用いてもよい．

#[derive(Serialize, Deserialize, Debug)]
struct Name {
    name: String,      // 別名 "オアシス"
    sort_name: String, // 別名（整列用） "オアシス"
}

#[derive(Serialize, Deserialize, Debug)]
struct Date {
    year: Option<isize>,  // 活動開始年 1991
    month: Option<isize>, // 活動開始月
    date: Option<isize>,  // 活動開始日
}

#[derive(Serialize, Deserialize, Debug)]
struct Tag {
    count: isize,  // タグ付けされた回数 1
    value: String, // タグ内容 "rock"
}

#[derive(Serialize, Deserialize, Debug)]
struct Rating {
    count: isize, // レーティングの投票数 13
    value: isize, // レーティングの値（平均値） 86
}

#[derive(Serialize, Deserialize, Debug)]
struct Artist {
    id: isize,                  // ユニーク識別子 20660
    gid: String,                // グローバル識別子 "ecf9f3a3-35e9-4c58-acaa-e707fba45060"
    name: String,               // アーティスト名 "Oasis"
    sort_name: String,          // アーティスト名（辞書順整列用） "Oasis"
    area: Option<String>,       // 活動場所 "United Kingdom"
    aliases: Option<Vec<Name>>, // 別名	辞書オブジェクトのリスト
    begin: Option<Date>,        // 活動開始日 辞書オブジェクト
    end: Option<Date>,          // 活動終了日 辞書オブジェクト
    tags: Option<Vec<Tag>>,     // タグ	辞書オブジェクトのリスト
    rating: Option<Rating>,     // レーティング 辞書オブジェクト
}

fn load() -> std::io::Result<Vec<Artist>> {
    let input = BufReader::new(File::open("artist.json")?);
    let data = Deserializer::from_reader(input).into_iter::<Value>();
    let mut vec: Vec<Artist> = Vec::new();
    for v in data {
        // let a: Artist = from_value(v?)?;
        // if let Some(r) = &a.rating {
        //     println!("{:?}", r);
        // }
        vec.push(from_value(v?)?);
    }
    Ok(vec)
}

/// dump the chopped json to import MongoDB Atlas
/// use:
/// ```
/// mongoimport ... --type json --jsonArray --file this.json
/// ```
fn dump(db: &Vec<Artist>) {
    let mut v = Vec::new();
    for a in db {
        let aliases = match &a.aliases {
            Some(v) => v.iter().map(|n| n.name.to_string()).collect(),
            None => vec![],
        };
        let tags = match &a.tags {
            Some(v) => v.iter().map(|t| t.value.to_string()).collect(),
            None => vec![],
        };
        let area = match &a.area {
            Some(p) => p.to_string(),
            None => "".to_string(),
        };
        let doc = json!({
            "name": a.name.to_string(),
            "aliases": aliases,
            "area": area,
            "tags": tags,
            "rating.count": match &a.rating {
                Some(r) => r.count as i64,
                None => 0i64,
            },
            "rating.value": match &a.rating {
                Some(r) => r.value as i64,
                None => 0i64,
            },
        });
        v.push(doc);
    }
    println!("{}", serde_json::to_string(&v).unwrap());
}
