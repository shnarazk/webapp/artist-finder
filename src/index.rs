use http::{header, Request, Response, StatusCode};
use mongodb::{bson, coll::{Collection, options}, db::ThreadedDatabase, doc, ThreadedClient};
use serde::Serialize;
use serde_json;
use std::env;
use url::form_urlencoded::parse;

struct DbQuery {
    name: String,
    place: String,
    tag: String,
}

impl Default for DbQuery {
    fn default() -> DbQuery {
        DbQuery {
            name: "".to_string(),
            place: "".to_string(),
            tag: "".to_string(),
        }
    }
}

fn handler(request: Request<()>) -> http::Result<Response<String>> {
    let uri_str = request.uri().to_string();
    let url = parse(uri_str.as_bytes());
    let mut query = DbQuery::default();
    for (k, v) in url {
        if let Some(key) = k.to_string().pop() {
            match key {
                'n' => query.name = v.to_string(),
                'p' => query.place = v.to_string(),
                't' => query.tag = v.to_string(),
                _ => (),
            }
        }
    }
    let response = 
        Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, "application/json")
        .body(build_json(&query))
        .expect("Failed to render response");
    Ok(response)
}

fn build_json(query: &DbQuery) -> String {
    match (env::var("MONGODB_ATLAS_URI"),
           env::var("MONGODB_ATLAS_USER"),
           env::var("MONGODB_ATLAS_PASSWD")) {
        (Ok(mongo), Ok(user), Ok(passwd)) => {
            let options = mongodb::ClientOptions::with_unauthenticated_ssl(None, false);
            let m = mongodb::Client::with_uri_and_options(&mongo, options).expect("connect");
            let mc = m.db("admin");
            mc.auth(&user, &passwd).expect("auth");
            let coll = m.db("test").collection("artists");
            make_response(&coll, &query)
        },
        _ => String::new(),
    }
}

#[derive(Serialize)]
struct Res {
    name: String,
    place: String,
    aliases: Vec<String>,
    tags: Vec<String>,
    rating_count: u64,
    rating_point: f64,
}

fn make_response(coll: &Collection, q: &DbQuery) -> String {
    let target =
        if q.place.is_empty() && q.tag.is_empty() {
            Some(doc! { "$or":
                         [
                             {
                                 "name": {
                                     "$regex": q.name.to_string(), "$options": "i"
                                 }
                             },
                             {
                                 "aliases": {
                                     "$regex": q.name.to_string(), "$options": "i"
                                 }
                             }
                         ]
            })
        } else if !q.place.is_empty() && !q.tag.is_empty() {
            Some(doc! { "$and":
                         [
                             { "$or":
                                [
                                    {
                                        "name": {
                                            "$regex": q.name.to_string(),
                                            "$options": "i"
                                        }
                                    },
                                    {
                                        "aliases": {
                                            "$regex": q.name.to_string(),
                                            "$options": "i"
                                        }
                                    }
                                ]
                             },
                             {
                                 "area": {
                                     "$regex": q.place.to_string(),
                                     "$options": "i"
                                 }
                             },
                             {
                                 "tags": {
                                     "$regex": q.tag.to_string(),
                                     "$options": "i"
                                 }
                             }
                         ] })
        } else if q.tag.is_empty() {
            Some(doc! { "$and":
                         [
                             { "$or":
                                [
                                    {
                                        "name": {
                                            "$regex": q.name.to_string(),
                                            "$options": "i"
                                        }
                                    },
                                    {
                                        "aliases": {
                                            "$regex": q.name.to_string(),
                                            "$options": "i"
                                        }
                                    }
                                ]
                             },
                             {
                                 "area": {
                                     "$regex": q.place.to_string(),
                                     "$options": "i"
                                 }
                             }
                         ] })
        } else {
            Some(doc! { "$and":
                         [
                             { "$or":
                                [
                                    {
                                        "name": {
                                            "$regex": q.name.to_string(),
                                            "$options": "i"
                                        }
                                    },
                                    {
                                        "aliases": {
                                            "$regex": q.name.to_string(),
                                            "$options": "i"
                                        }
                                    }
                                ]
                             },
                             {
                                 "tags": {
                                     "$regex": q.tag.to_string(),
                                     "$options": "i"
                                 }
                             }
                         ] })
        };
    let mut opts = options::FindOptions::new();
    opts.limit = Some(800);
    match coll.find(target, Some(opts)) {
        Ok(cursor) => {
            let mut vec: Vec<(i64, bson::ordered::OrderedDocument)> = Vec::new();
            for result in cursor {
                if let Ok(item) = result {
                    let mut val: i64 =  0;
                    if let Some(c) = item.get("rating.count") {
                        val -= 20 * c.as_i64().unwrap_or(0);
                    }
                    if let Some(v) = item.get("rating.value") {
                        val -= 10 * v.as_i64().unwrap_or(0);
                    }
                    if let Some(t) = item.get("tags") {
                        val -= 15 * t.as_array().map_or(0, |v| v.len() as i64);
                    }
                    if let Some(a) = item.get("aliases") {
                        val -= 10 * a.as_array().map_or(0, |v| v.len() as i64);
                    }
                    if let Some(_) = item.get("place") {
                        val -= 20;
                    }
                    vec.push((val, item));
                }
            }
            vec.sort_by_key(|c| (c.0 as isize));
            let res = vec.iter()
                .take(40)
                .map(|(_, a)| {
                    Res {
                        name: a.get("name")
                            .and_then(|x| x.as_str())
                            .unwrap_or("")
                            .to_string(),
                        place: a.get("area")
                            .and_then(|x| x.as_str())
                            .unwrap_or("")
                            .to_string(),
                        aliases: a.get("aliases")
                            .and_then(|t| t.as_array())
                            .map_or(
                                vec![],
                                |v| v.iter()
                                    .map(|x| x.as_str().unwrap_or("").to_string())
                                    .collect::<Vec<String>>()),
                        tags: a.get("tags")
                            .and_then(|t| t.as_array())
                            .map_or(
                                vec![],
                                |v| v.iter()
                                    .map(|x| x.as_str().unwrap_or("").to_string())
                                    .collect::<Vec<String>>()),
                        rating_count: a.get("rating.count")
                            .and_then(|x| x.as_str())
                            .and_then(|y| y.parse().ok())
                            .unwrap_or(0),
                        rating_point: a.get("rating.value")
                            .and_then(|x| x.as_str())
                            .and_then(|y| y.parse().ok())
                            .unwrap_or(0.0),
                    }
                })
                .collect::<Vec<Res>>();
            return serde_json::to_string(&res).unwrap();
        }
        Err(_) => "".to_string(),
    }
}
